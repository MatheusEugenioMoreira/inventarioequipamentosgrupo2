## Projeto: Fundamentos de Sistemas - Entrega Etapa 3

### Grupo 2

#### Membros

| Nome                    | RA      |
| ----------------------- | ------- |
| Matheus Eugênio Moreira | 453592  |
| Flávia Évanlen          | 1516180 |
| Matheus Junio           | 1526870 |
| Maria Eduarda           | 1510644 |
| Otávio Freitas          | 1520053 |
| Pedro Del Gruercio      | 1521479 |

Para executar o projeto em sua máquina, utilize o comando abaixo:

```shellscript
dotnet run
```

Para compilar o projeto, utilize o comando abaixo:

```shellscript
dotnet build
```
