﻿/*
Projeto: Fundamentos de Sistemas - Etapa 3
Grupo 2
Matheus Eugênio - 453592
Flávia Évanlen - 1516180
Matheus Junio – 1526870
Maria Eduarda – 1510644
Otávio Freitas – 1520053
Pedro Del Gruercio – 1521479
*/

using System;
using Microsoft.Data.Sqlite;

namespace InventoryApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string stringConexaoBD = "Data Source=inventarioGrupo2.db";
            CriaBancoDeDados(stringConexaoBD);

            Console.WriteLine("Bem vindo ao programa de inventário de equipamentos da ValeCard");
            Console.Write("Antes de prosseguir, me informe o seu nome: ");
            string autor = Console.ReadLine();

            while (true)
            {
                Console.Clear();
                Console.WriteLine("Selecione uma opção:\n");
                Console.WriteLine("1 - Cadastrar um novo Hardware");
                Console.WriteLine("2 - Cadastrar um novo Software");
                Console.WriteLine("3 - Cadastrar um novo Hardware e Software");
                Console.WriteLine("4 - Listar equipamentos ativos");
                Console.WriteLine("5 - Listar equipamentos excluídos");
                Console.WriteLine("6 - Excluir um equipamento");
                Console.WriteLine("7 - Quantidade de equipamentos cadastrados por usuário");
                Console.WriteLine("0 - Sair\n");
                string opcao = Console.ReadLine();

                if (opcao == "0")
                {
                    break;
                }

                switch (opcao)
                {
                    case "1":
                    case "2":
                    case "3":
                        RenderizaMenu(opcao, autor, stringConexaoBD);
                        break;
                    case "4":
                        ListarDadosBD(stringConexaoBD, false);
                        break;
                    case "5":
                        ListarDadosBD(stringConexaoBD, true);
                        break;
                    case "6":
                        DeletarDadoBD(stringConexaoBD);
                        break;
                    case "7":
                        QuantitativoPorAutor(stringConexaoBD);
                        break;
                    default:
                        Console.WriteLine("Opção inválida. Tente novamente.");
                        break;
                }
            }
        }

        static void CriaBancoDeDados(string stringConexaoBD)
        {
            using (var conexao = new SqliteConnection(stringConexaoBD))
            {
                conexao.Open();
                string query = @"create table if not exists inventarioGrupo2 (
                                            id integer primary key autoincrement,
                                            codigo text not null,
                                            modelo text not null,
                                            fabricante text not null,
                                            tipo text not null,
                                            autor text not null,
                                            excluido bit not null default 0);";

                using (var comando = new SqliteCommand(query, conexao))
                {
                    comando.ExecuteNonQuery();
                }
            }
        }

        static void RenderizaMenu(string opcao, string autor, string stringConexaoBD)
        {
            string tipo = opcao switch
            {
                "1" => "Hardware",
                "2" => "Software",
                "3" => "Hardware e Software",
                _ => throw new ArgumentException("Opção inválida", nameof(opcao))
            };

            Console.WriteLine($"Preencha os dados que serão solicitados abaixo para prosseguir com o cadastro do novo {tipo}:");
            Console.Write("Código: ");
            string codigo = Console.ReadLine();
            Console.Write("Modelo: ");
            string modelo = Console.ReadLine();
            Console.Write("Fabricante: ");
            string fabricante = Console.ReadLine();

            SaveToDatabase(stringConexaoBD, codigo, modelo, fabricante, tipo, autor);

            Console.WriteLine($"Novo {tipo} salvo com sucesso!");
            Console.WriteLine("Pressione qualquer tecla para retornar ao menu.");
            Console.ReadKey();
        }

        static void SaveToDatabase(string stringConexaoBD, string codigo, string modelo, string fabricante, string tipo, string autor)
        {
            using (var conexao = new SqliteConnection(stringConexaoBD))
            {
                conexao.Open();
                string query = @"insert into inventarioGrupo2 (codigo, modelo, fabricante, tipo, autor, excluido)
                                       values (@codigo, @modelo, @fabricante, @tipo, @autor, 0)";

                using (var comando = new SqliteCommand(query, conexao))
                {
                    comando.Parameters.AddWithValue("@codigo", codigo);
                    comando.Parameters.AddWithValue("@modelo", modelo);
                    comando.Parameters.AddWithValue("@fabricante", fabricante);
                    comando.Parameters.AddWithValue("@tipo", tipo);
                    comando.Parameters.AddWithValue("@autor", autor);
                    comando.ExecuteNonQuery();
                }
            }
        }

        static void ListarDadosBD(string stringConexaoBD, bool excluded)
        {
            using (var conexao = new SqliteConnection(stringConexaoBD))
            {
                conexao.Open();
                string query = "select * from inventarioGrupo2 where excluido = @excluido";

                using (var comando = new SqliteCommand(query, conexao))
                {
                    comando.Parameters.AddWithValue("@excluido", excluded ? 1 : 0);

                    using (var linha = comando.ExecuteReader())
                    {
                        Console.WriteLine("ID\tCódigo\t\tModelo\t\tFabricante\t\tTipo\t\tAutor");
                        Console.WriteLine("------------------------------------------------------------------------------------------");

                        while (linha.Read())
                        {
                            Console.WriteLine($"{linha["id"],-5}\t{linha["codigo"],-15}\t{linha["modelo"],-15}\t{linha["fabricante"],-20}\t{linha["tipo"],-15}\t{linha["autor"],-15}");
                        }
                    }
                }
                Console.WriteLine("\nPressione qualquer tecla para continuar...");
                Console.ReadKey();
            }
        }

        static void QuantitativoPorAutor(string stringConexaoBD)
        {
            using (var conexao = new SqliteConnection(stringConexaoBD))
            {
                conexao.Open();
                string query = "select autor, count(1) as qta_equipamentos from inventarioGrupo2 group by autor";

                using (var comando = new SqliteCommand(query, conexao))
                {

                    using (var linha = comando.ExecuteReader())
                    {
                        Console.WriteLine("Autor\t\tQuantidade");
                        Console.WriteLine("--------------------------");

                        while (linha.Read())
                        {
                            Console.WriteLine($"{linha["autor"],-15}{linha["qta_equipamentos"],-10}");
                        }
                    }
                }
                Console.WriteLine("\nPressione qualquer tecla para continuar...");
                Console.ReadKey();
            }
        }

        static void DeletarDadoBD(string stringConexaoBD)
        {
            Console.Write("Digite o ID do equipamento que deseja excluir: ");
            if (int.TryParse(Console.ReadLine(), out int id))
            {
                using (var conexao = new SqliteConnection(stringConexaoBD))
                {
                    conexao.Open();
                    string query = @"update inventarioGrupo2 set excluido = 1 where id = @id";

                    using (var comando = new SqliteCommand(query, conexao))
                    {
                        comando.Parameters.AddWithValue("@id", id);
                        int linhasAfetadas = comando.ExecuteNonQuery();

                        if (linhasAfetadas > 0)
                        {
                            Console.WriteLine("Equipamento excluído com sucesso!");
                        }
                        else
                        {
                            Console.WriteLine("Equipamento não encontrado.");
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("ID inválido.");
            }
            Console.WriteLine("Pressione qualquer tecla para continuar...");
            Console.ReadKey();
        }
    }
}
